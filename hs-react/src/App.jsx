import Home from "./components/Home";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Search from "./components/Search";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/search" element={<Search />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
