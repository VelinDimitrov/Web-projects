import axios from "axios";
import constants from "../constants";

export const getCards = (filters = { race: "Quilboar", maxResults: 30 }) => {
  const { race, ...otherFilters } = filters;

  Object.keys(otherFilters).forEach(
    (key) => otherFilters[key] === "" && delete otherFilters[key]
  );
  return axios.get(`${constants.baseUrl}/cards/races/${filters.race}`, {
    ...constants.settings,
    params: otherFilters,
  });
};
