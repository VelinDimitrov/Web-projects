import { useContext, useEffect, useState } from "react";
import { getCards } from "../../api/cards";
import { FilterContext } from "../providers/FilterProvider";

export const useCards = () => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const [cards, setCards] = useState([]);
  const { filters } = useContext(FilterContext);

  useEffect(() => {
    setLoading(true);
    setError("");
    getCards(filters)
      .then((response) => {
        if (response?.data) {
          const processedCards = response.data
            .filter((card) => card.img)
            .reduce(
              (accumulator, card) =>
                accumulator.some((c) => c.name === card.name)
                  ? accumulator
                  : [...accumulator, card],
              []
            )
            .slice(0, filters.maxResults);
          setCards(processedCards);
        }
        setLoading(false);
      })
      .catch((error) => {
        setError(error.response.data.message);
        setLoading(false);
      });
  }, [filters]);

  return { cards, loading, error };
};
