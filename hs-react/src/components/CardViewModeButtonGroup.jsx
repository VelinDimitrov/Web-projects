import { useContext } from "react";
import { Button, ButtonGroup } from "react-bootstrap";
import { CardViewModeContext } from "./providers/CardViewModeProvider";

const CardViewModeButtonGroup = () => {
  const { mode, setMode } = useContext(CardViewModeContext);

  return (
    <ButtonGroup role="group" aria-label="Switch view">
      <Button
        id="list-view"
        variant={mode === "LIST" ? "primary" : "outline-primary"}
        onClick={() => setMode("LIST")}
      >
        <i className="fas fa-th-list" />
      </Button>
      <Button
        id="grid-view"
        variant={mode === "GRID" ? "primary" : "outline-primary"}
        onClick={() => setMode("GRID")}
      >
        <i className="fas fa-grip-horizontal" />
      </Button>
    </ButtonGroup>
  );
};

export default CardViewModeButtonGroup;
