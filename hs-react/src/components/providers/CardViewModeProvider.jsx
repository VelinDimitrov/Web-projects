import React, { useState } from "react";

export const CardViewModeContext = React.createContext();

export const CardViewModeProvider = (props) => {
  const [mode, setMode] = useState("LIST");

  return (
    <CardViewModeContext.Provider value={{ mode, setMode }}>
      {props.children}
    </CardViewModeContext.Provider>
  );
};
