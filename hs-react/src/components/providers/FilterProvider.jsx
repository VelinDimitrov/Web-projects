import React, { useState } from "react";

export const FilterContext = React.createContext();

export const FilterProvider = (props) => {
  const [filters, setFilters] = useState({ race: "Quilboar", maxResults: 30 });

  const onFilterChange = (name, newValue) =>
    setFilters((prevFilters) => ({ ...prevFilters, [name]: newValue }));

  return (
    <FilterContext.Provider value={{ filters, onFilterChange }}>
      {props.children}
    </FilterContext.Provider>
  );
};
