import styled from "styled-components";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <Container>
      <Link to="/search">
        <MoreImage
          className="border border-dark"
          src="/images/More-button.PNG"
          alt="HS visualisation"
        />
      </Link>
    </Container>
  );
};

const MoreImage = styled.img`
  position: absolute;
  left: 0;
  right: 0;
  margin: 0 auto;
  bottom: 1rem;
  transition: all 0.2s ease-in-out;
  :hover {
    transform: scale(1.2);
    cursor: pointer;
  }
`;

const Container = styled.div`
  background-image: url("/images/index-bg.jpg");
  background-size: 100% 100%;
  height: 100%;
  position: relative;
`;
export default Home;
