import styled from "styled-components";

const LoadingSpinner = () => (
  <Wrapper>
    <div className="spinner-border">
      <span className="visually-hidden">Loading...</span>
    </div>
  </Wrapper>
);

export default LoadingSpinner;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 0.75rem;
`;
