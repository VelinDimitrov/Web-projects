import { useContext } from "react";
import { Card, Col, FormLabel, Form } from "react-bootstrap";
import styled from "styled-components";
import { FilterContext } from "./providers/FilterProvider";

const races = [
  "Quilboar",
  "Murloc",
  "Demon",
  "Mech",
  "Dragon",
  "Pirate",
  "Totem",
  "Beast",
  "Elemental",
];

const Filters = () => {
  const { filters, onFilterChange } = useContext(FilterContext);
  return (
    <Col md="3">
      <StyledCar>
        <Card.Body>
          <CardTitle>Filters</CardTitle>
          <Form>
            <SelectWrapper>
              <FormLabel htmlFor="race-filter">Race</FormLabel>
              <Form.Select
                id="race-filter"
                value={filters.race}
                onChange={(e) => onFilterChange("race", e.target.value)}
              >
                {races.map((race) => (
                  <option key={race}>{race}</option>
                ))}
              </Form.Select>
            </SelectWrapper>

            <SplitRowFilterContainer>
              <SplitRowFilter>
                <FormLabel htmlFor="card-attack">Attack</FormLabel>
                <Form.Control
                  id="card-attack"
                  type="number"
                  min="1"
                  value={filters.attack}
                  onChange={(e) => onFilterChange("attack", e.target.value)}
                />
              </SplitRowFilter>
              <SplitRowFilter>
                <FormLabel htmlFor="card-health">Health</FormLabel>
                <Form.Control
                  id="card-health"
                  type="number"
                  min="1"
                  value={filters.health}
                  onChange={(e) => onFilterChange("health", e.target.value)}
                />
              </SplitRowFilter>
            </SplitRowFilterContainer>

            <SplitRowFilterContainer>
              <SplitRowFilter>
                <FormLabel htmlFor="card-cost">Cost</FormLabel>
                <Form.Control
                  id="card-cost"
                  type="number"
                  min="1"
                  value={filters.cost}
                  onChange={(e) => onFilterChange("cost", e.target.value)}
                />
              </SplitRowFilter>
              <SplitRowFilter>
                <FormLabel htmlFor="max-result">Max Results</FormLabel>
                <Form.Control
                  id="max-result"
                  type="number"
                  min="1"
                  value={filters.maxResults}
                  onChange={(e) => onFilterChange("maxResults", e.target.value)}
                />
              </SplitRowFilter>
            </SplitRowFilterContainer>
          </Form>
        </Card.Body>
      </StyledCar>
    </Col>
  );
};

const CardTitle = styled(Card.Title)`
  margin-bottom: 1rem;
`;

const SelectWrapper = styled.div`
  margin-bottom: 1rem;
`;

const StyledCar = styled(Card)`
  background-color: #e3f2fd;
`;

const SplitRowFilterContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

const SplitRowFilter = styled.div`
  display: flex;
  flex-direction: column;
  width: 48%;
`;

export default Filters;
