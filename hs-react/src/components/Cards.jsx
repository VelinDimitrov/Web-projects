import { Card, Col, Row, Alert } from "react-bootstrap";
import styled from "styled-components";
import CardsHeader from "./CardsHeader";
import { useCards } from "./hooks/useCards";
import HSCard from "./HSCard";
import LoadingSpinner from "./LoadingSpinner";

const Cards = () => {
  const { cards, loading, error } = useCards();
  return (
    <Col>
      <StyledCard>
        <Card.Body>
          <CardsHeader />
          <hr />
          {loading && <LoadingSpinner />}
          {!loading && (error || cards?.length === 0) && (
            <Alert variant="danger" role="alert">
              {error ?? "There are no cards for the chosen filters"}
            </Alert>
          )}
          <Row>
            {!error &&
              !loading &&
              cards?.length > 0 &&
              cards.map((card) => <HSCard card={card} key={card.cardId} />)}
          </Row>
        </Card.Body>
      </StyledCard>
    </Col>
  );
};

const StyledCard = styled(Card)`
  background-color: #e3f2fd;
`;

export default Cards;
