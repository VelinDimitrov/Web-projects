import { Card } from "react-bootstrap";
import styled from "styled-components";
import CardViewModeButtonGroup from "./CardViewModeButtonGroup";

const CardsHeader = () => (
  <Header>
    <Card.Title>Cards/Races</Card.Title>
    <CardViewModeButtonGroup />
  </Header>
);

export default CardsHeader;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 0.75rem;
`;
