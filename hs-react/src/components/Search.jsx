import { Container, Row } from "react-bootstrap";
import Cards from "./Cards";
import Filters from "./Filters";
import Footer from "./Footer";
import NavBar from "./NavBar";
import styled from "styled-components";
import { FilterProvider } from "./providers/FilterProvider";
import { CardViewModeProvider } from "./providers/CardViewModeProvider";

const Search = () => {
  return (
    <CardViewModeProvider>
      <FilterProvider>
        <NavBar />

        <Container>
          <MarginedRow>
            <Filters />
            <Cards />
          </MarginedRow>

          <Footer />
        </Container>
      </FilterProvider>
    </CardViewModeProvider>
  );
};

const MarginedRow = styled(Row)`
  margin-top: 2rem;
  margin-bottom: 1.5rem;
`;
export default Search;
