import styled from "styled-components";

const Footer = () => (
  <FooterWrapper>
    <div>
      <FooterText className="text-muted">
        Veli OOD © 2021 Company, Inc
      </FooterText>
    </div>
  </FooterWrapper>
);

const FooterText = styled.span`
  color: #6c757d;
`;

const FooterWrapper = styled.footer`
  display: flex;
  border-top: 1px solid #dee2e6;
  padding-top: 1rem;
  padding-bottom: 1rem;
  margin-bottom: 1.5rem;
  margin-top: 1.5rem;
`;

export default Footer;
