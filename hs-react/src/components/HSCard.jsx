import { useContext } from "react";
import { Badge, Col } from "react-bootstrap";
import styled from "styled-components";
import { CardViewModeContext } from "./providers/CardViewModeProvider";

const GridCard = ({ card }) => {
  const description =
    card.text &&
    card.text
      .replaceAll("\\n", " ")
      .replaceAll("_", " ")
      .replaceAll("[x]", " ");
  return (
    <Col md="4">
      <GridCardContainer>
        <div className="d-flex">
          <CardGridImage src={card.img ?? ""} />
          <div className="px-2 w">
            <div className="fw-bold py-1">{card.name}</div>
            <Badge bg="primary" pill>
              {card.race}
            </Badge>
            <TopPaddedDiv>{card.cardSet}</TopPaddedDiv>
          </div>
        </div>
        <TopPaddedDiv dangerouslySetInnerHTML={{ __html: description }} />
      </GridCardContainer>
    </Col>
  );
};

const CardGridImage = styled.img`
  width: 15vh;
`;

const GridCardContainer = styled.div`
  height: 300px;
  overflow: hidden;
  margin-bottom: 1rem;
`;

const ListCard = ({ card }) => {
  const description =
    card.text &&
    card.text
      .replaceAll("\\n", " ")
      .replaceAll("_", " ")
      .replaceAll("[x]", " ");
  return (
    <>
      <CardWrapper>
        <CardImage src={card.img ?? ""} />
        <CardContentWrapper>
          <CardHeader>
            <b>{card.name}</b>
            <RightAlignedBadge bg="primary" pill>
              {card.race}
            </RightAlignedBadge>
          </CardHeader>
          <TopPaddedDiv>{card.cardSet}</TopPaddedDiv>
          <TopPaddedDiv dangerouslySetInnerHTML={{ __html: description }} />
        </CardContentWrapper>
      </CardWrapper>
      <MarginedLine />
    </>
  );
};

const CardContentWrapper = styled.div`
  padding: 0 1rem;
  flex-grow: 1;
`;

const CardHeader = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
`;

const CardWrapper = styled.div`
  display: flex;
  padding: 1rem 0;
`;

const MarginedLine = styled.hr`
  margin: 0.25rem 0;
`;

const TopPaddedDiv = styled.div`
  padding-top: 1rem;
`;

const CardImage = styled.img`
  width: 15%;
`;

const RightAlignedBadge = styled(Badge)`
  margin-left: auto;
`;

const HSCard = ({ card }) => {
  const { mode } = useContext(CardViewModeContext);
  return mode === "LIST" ? <ListCard card={card} /> : <GridCard card={card} />;
};

export default HSCard;
