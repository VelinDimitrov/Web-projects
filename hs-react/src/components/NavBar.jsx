import { Container, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import styled from "styled-components";

const NavBar = () => {
  return (
    <Container>
      <StyledNavBar>
        <Container fluid>
          <BrandLink to="/">
            <Brand src="/images/hs-icon.png" alt="" width="30" height="30" />
            <BrandText>Home</BrandText>
          </BrandLink>
        </Container>
      </StyledNavBar>
    </Container>
  );
};

const Brand = styled.img`
  display: inline-block;
  vertical-align: text-top;
`;

const BrandLink = styled(Link)`
  color: black;
  padding-top: 0.3125rem;
  padding-bottom: 0.3125rem;
  margin-right: 1rem;
  font-size: 1.25rem;
  text-decoration: none;
  white-space: nowrap;

  :hover {
    background: #dbdbdb;
    font-weight: bold;
    color: black;
  }
`;

const BrandText = styled.span`
  margin-left: 0.5rem;
`;
const StyledNavBar = styled(Navbar)`
  background-color: #e3f2fd;
`;

export default NavBar;
