const constants = {
  baseUrl: "https://omgvamp-hearthstone-v1.p.rapidapi.com",
  settings: {
    headers: {
      "x-rapidapi-host": "omgvamp-hearthstone-v1.p.rapidapi.com",
      "x-rapidapi-key": "4cc1ae9b92msh7bc8cd18d16cd6bp164c24jsn883e0206bdb5",
    },
    async: true,
    crossDomain: true,
  },
};
export default constants;
