import React from "react";

export const AksialPumps = ({ messagesAksialPumps }) => (
  <div class="pb-3" ref={messagesAksialPumps}>
    <h1 class="display-4 hydraulic-pumps">
      Аксиално бутални хидравлични помпи
    </h1>
    <p class="lead">
      Фирмата извършва ремонт на всички видове хидравлични помпи и хидро мотори.
      Ние не правим само преглед на Вашата хидравлична помпа, а подменяме всички
      износващи се компоненти в нея с нови, а при невъзможност се изработват с
      прецизност при предварително обсъждане с клиента за оптимално
      удволетворяване на изискванията и получените резултати.
    </p>
    <img
      class="d-block img-fluid"
      src="./img/autoshop/aksialno-butalni-pompi.png"
      alt="sf"
    />
  </div>
);
