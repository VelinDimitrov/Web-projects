import React from "react";
import { BigCarouselSpec } from "./BigCarouselSpec";
import { LowerCarouselSpec } from "./LowerCarouselSpec";
import { RightCarouselRows } from "./RightCarouselRows";

const bigSpecInfo = [
  { name: "Ремонт на кранове", src: "./img/rsz_crane.jpg" },
  { name: "Ремонт на вишки", src: "./img/rsz_haulotte.jpg" },
  { name: "Ремонт на трактори", src: "./img/rsz_tracktor.jpg" },
];

const middleSpecInfo = [
  { name: "Ремонт на коли", src: "./img/box-1.jpeg" },
  { name: "Ремонт на скоростни кутии, диференциали", src: "./img/gear.jpeg" },
  { name: "Ремонт на двигатели, рейки", src: "./img/engine.jpeg" },
  { name: "ГНП Дюзи", src: "./img/nozzle.jpg" },
];
const lowerSpecInfo = [
  { name: "Ремонт на Хидравлични бордове", src: "./img/bord.jpg" },
  { name: "Цилиндри, разпределителни клапани", src: "./img/engine.jpeg" },
  { name: "Хидравлични помпи", src: "./img/hydraulicpump.jpg" },
  { name: "ПТО редуктори", src: "./img/box-1.jpeg" },
];

export const Specialisation = () => {
  return (
    <div className="container-fluid py-4">
      <div className="row mb-4">
        <BigCarouselSpec bigSpecInfo={bigSpecInfo} />
        <RightCarouselRows middleSpecInfo={middleSpecInfo} />
      </div>
      <div className="row lower-row">
        {lowerSpecInfo &&
          lowerSpecInfo.map((carousel) => {
            return (
              <LowerCarouselSpec
                key={carousel.name}
                name={carousel.name}
                src={carousel.src}
              />
            );
          })}
      </div>
    </div>
  );
};
