import React from "react";
import { Link } from "react-router-dom";
import { useLocation } from "react-router";

export const NavBar = ({ scrollToBottom }) => {
  const location = useLocation();

  const pathName = location.pathname;
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <Link to="/" className="navbar-brand">
        SanirLTD
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav m-auto">
          <li className={`nav-item px-3 ${pathName === "/" ? "active" : ""}`}>
            <Link to="/" className="nav-link">
              Начало <span className="sr-only">(current)</span>
            </Link>
          </li>
          <li
            className={`nav-item px-3 ${pathName === "/about" ? "active" : ""}`}
          >
            <Link to="/about" className="nav-link">
              За Нас
            </Link>
          </li>
          <li
            className={`nav-item px-3 ${pathName === "/shop" ? "active" : ""}`}
          >
            <Link to="/shop" className="nav-link">
              Сервиз
            </Link>
          </li>
          <li
            className={`nav-item px-3 ${
              pathName === "/partners" ? "active" : ""
            }`}
          >
            <Link to="/partners" className="nav-link">
              Партньори
            </Link>
          </li>
          <li className="nav-item px-3">
            <Link to="#" className="nav-link" onClick={scrollToBottom}>
              Контакти
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};
