import React from "react";

export const HydroMortors = ({ messagesHydroMotors }) => (
  <React.Fragment>
    <div class="col" ref={messagesHydroMotors}>
      <h1 class="display-5 pumps">Хидравлични помпи, хидро-мотори</h1>
      <p class="lead">
        Фирмата извършва ремонт на всички видове хидравлични помпи и хидро
        мотори. Ние не правим само преглед на Вашата хидравлична помпа, а
        подменяме всички износващи се компоненти в нея с нови,а при невъзможност
        се изработват с прецизност при предварително обсъждане с клиента за
        оптимално удволетворяване на изискванията и получените резултати.
      </p>
    </div>
    <div class="col motors">
      <span class="d-block text-muted py-3">
        Аксиално-бутални помпи и мотори
      </span>
      <img class="d-block img-fluid" src="./img/autoshop/pompi1.png" alt="sf" />
    </div>
    <div class="col">
      <span class="d-block text-muted py-3">
        Радиално-бутални помпи и мотори
      </span>
      <img class="d-block img-fluid" src="./img/autoshop/pompi2.jpg" alt="sf" />
    </div>
    <div class="col">
      <span class="d-block text-muted py-3">
        Пластинчати помпи с променлива геометрия
      </span>
      <img class="d-block img-fluid" src="./img/autoshop/pompi3.jpg" alt="sf" />
    </div>
    <div class="col">
      <span class="d-block text-muted py-3">Зъбни помпи и мотори</span>
      <img class="d-block img-fluid" src="./img/autoshop/pompi4.jpg" alt="sf" />
    </div>
    <div class="col">
      <span class="d-block text-muted py-3">
        Много-тактови хидромотори за валяци и други{" "}
      </span>
      <img class="d-block img-fluid" src="./img/autoshop/pompi5.jpg" alt="sf" />
    </div>
    <div class="col">
      <span class="d-block text-muted py-3">Орбитални мотори</span>
      <img class="d-block img-fluid" src="./img/autoshop/pompi6.jpg" alt="sf" />
    </div>
    <div class="col">
      <p class="lead text-muted py-3">
        Пълна гама хидравлични орбитални мотори за приложение в мобилни и
        индустриални системи с геометричен обем от 10 до 900 cm3 от следните
        марки:
      </p>
      <ul class="list-unstyled">
        <li class="list-group-item">REXTROTH</li>
        <li class="list-group-item">LIEBHERR</li>
        <li class="list-group-item">SAUER DANFOSS</li>
        <li class="list-group-item">POCLAIN HYDRAULICS</li>
        <li class="list-group-item">PARKER HANNIFIN</li>
        <li class="list-group-item">VOLVO</li>
        <li class="list-group-item">VICKERS</li>
        <li class="list-group-item">DENISON HYDRAULIK</li>
        <li class="list-group-item">MOOG</li>
        <li class="list-group-item">VOITH</li>
        <li class="list-group-item">EATON</li>
        <li class="list-group-item">ORSTA HYDRAULIK</li>
      </ul>
      <p class="lead text-muted py-2">на следната мобилна техника</p>
      <ul class="list-unstyled">
        <li class="list-group-item">LIEBHERR</li>
        <li class="list-group-item">CATERPILLAR</li>
        <li class="list-group-item">KOMATSU</li>
        <li class="list-group-item">VOLVO</li>
        <li class="list-group-item">CASE POCLAIN</li>
        <li class="list-group-item">HITACHI</li>
        <li class="list-group-item">CLAAS</li>
        <li class="list-group-item">VÖGELE</li>
        <li class="list-group-item">JCB</li>
        <li class="list-group-item">JOHN DEERE и други</li>
      </ul>
    </div>
  </React.Fragment>
);
