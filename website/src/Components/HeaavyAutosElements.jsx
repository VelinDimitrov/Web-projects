import React from "react";

export const HeaavyAutosElements = ({ messagesHeavyAutosElement }) => (
  <div class="col" ref={messagesHeavyAutosElement}>
    <h1 class="display-4 automobiles">
      РЕМОНТ НА ЕЛЕМЕНТИ НА ТОВАРНИ АВТОМОБИЛИ И РЕМАРКЕТА
    </h1>
    <h3 class="text-muted">
      Имаме възможност за извърване на следните технически операции:
    </h3>
    <ul class="list-unstyled">
      <li class="list-group-item">
        Елементи на окачването-листови или пружинни ресори, амортисьори,
        конзоли, греди на мостовете и осите, стабилизиращи щанги, всякакви
        видове тампони и втулки.
      </li>
      <li class="list-group-item">Скорости с хидрортансформатор или ръчни.</li>
      <li class="list-group-item">Диференциали.</li>
      <li class="list-group-item">
        Основен ремонт двигатели с вътрешно горене.
      </li>
      <li class="list-group-item">
        Диагностика,ремонт и монтаж на елементи на падащи бордове,гаранционно и
        изван гаранционно обслужване..
      </li>
      <li class="list-group-item">
        Диагностика и ремонт на основни възли и агрегати.
      </li>
      <li class="list-group-item">
        Диагностика и ремонт на горивни системи – дизелови.
      </li>
      <li class="list-group-item">
        Диагностика и ремонт на ел. инсталации и електронни системи.
      </li>
      <li class="list-group-item">
        Диагностика и ремонт на пневматични и хидравлични системи в транспортна
        или подемна машина.
      </li>
      <li class="list-group-item">
        Диагностика и основен ремонт на елементи на падащи бордове, както на
        хидравликата, така и на електроника или електро схемата на работа.
        <strong>Гаранционно и извънгаранционно обслужване.</strong>
      </li>
      <li class="list-group-item">Преглед на техника при покупко-продажба.</li>
    </ul>
  </div>
);
