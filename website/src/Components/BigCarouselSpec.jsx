import React from "react";

export const BigCarouselSpec = ({ bigSpecInfo }) => {
  return (
    <div className="col-lg-6 col-md-12 col-sm-12 col-lg-6-special">
      <div
        id="slider1"
        className="col-lg-12 carousel slide"
        data-ride="carousel"
      >
        <div className="carousel-inner" role="listbox">
          <div className="carousel-item active img-overlay-box">
            <img
              className="w-100 h-100 img-thumbnail d-block img-fluid"
              src={bigSpecInfo[0].src}
              alt="img1"
            />
            <div className="carousel-caption">
              <h1 className="display-3 my-5">{bigSpecInfo[0].name}</h1>
            </div>
          </div>
          <div className="carousel-item img-overlay-box">
            <img
              className="w-100 h-100 img-thumbnail d-block img-fluid"
              src={bigSpecInfo[1].src}
              alt="img2"
            />
            <div className="carousel-caption">
              <h1 className="display-3 my-5">{bigSpecInfo[1].name}</h1>
            </div>
          </div>
          <div className="carousel-item img-overlay-box">
            <img
              className="w-100 h-100 img-thumbnail d-block img-fluid"
              src={bigSpecInfo[2].src}
              alt="img3"
            />
            <div className="carousel-caption">
              <h1 className="display-3 my-5">{bigSpecInfo[2].name}</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
