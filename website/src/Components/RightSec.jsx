import React from "react";
import { Link } from "react-router-dom";

export const RightSec = ({ src, alt, name }) => (
  <div className="col-md-6 col-xs-12">
    <div class="hovereffect">
      <img
        className="w-100 h-100 img-thumbnail d-block img-fluid"
        src={src}
        alt={alt}
      />
      <div class="overlay">
        <h2>{name}</h2>
        <Link className="info" to={"/shop"}>
          Отведи ме
        </Link>
      </div>
    </div>
  </div>
);
