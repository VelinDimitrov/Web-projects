import React, { useCallback, useRef } from "react";
import { AdditionalInformation } from "./AdditionalInformation";
import { AksialPumps } from "./AksialPumps";
import { AutoShopSideBar } from "./AutoShopSideBar";
import { DieselInjector } from "./DieselInjector";
import { HeaavyAutosElements } from "./HeaavyAutosElements";
import { HydraulicService } from "./HydraulicService";
import { HydroMortors } from "./HydroMortors";
import { SteeringHydraulics } from "./SteeringHydraulics";
import { TechnicalOperations } from "./TechnicalOperations";

export const AutoShop = ({ scrollToTop }) => {
  const messagesAksialPumps = useRef(null);
  const scrollToAksialPumps = useCallback(() => {
    messagesAksialPumps.current.scrollIntoView({ behavior: "smooth" });
  }, [messagesAksialPumps]);

  const messagesSteeringHydraulics = useRef(null);
  const scrollToSteeringHydraulics = useCallback(() => {
    messagesSteeringHydraulics.current.scrollIntoView({ behavior: "smooth" });
  }, [messagesSteeringHydraulics]);

  const messagesDieselInjector = useRef(null);
  const scrollToDieselInjector = useCallback(() => {
    messagesDieselInjector.current.scrollIntoView({ behavior: "smooth" });
  }, [messagesDieselInjector]);

  const messagesHydroMotors = useRef(null);
  const scrollToHydroMotors = useCallback(() => {
    messagesHydroMotors.current.scrollIntoView({ behavior: "smooth" });
  }, [messagesHydroMotors]);

  const messagesHeavyAutosElement = useRef(null);
  const scrollToHeavyAutosElement = useCallback(() => {
    messagesHeavyAutosElement.current.scrollIntoView({ behavior: "smooth" });
  }, [messagesHeavyAutosElement]);

  const messagesHydraulicsService = useRef(null);
  const scrollToHydraulicsService = useCallback(() => {
    messagesHydraulicsService.current.scrollIntoView({ behavior: "smooth" });
  }, [messagesHydraulicsService]);

  return (
    <div className="container-fluid">
      <div className="row">
        <AutoShopSideBar
          scrollToAksialPumps={scrollToAksialPumps}
          scrollToSteeringHydraulics={scrollToSteeringHydraulics}
          scrollToDieselInjector={scrollToDieselInjector}
          scrollToHydroMotors={scrollToHydroMotors}
          scrollToHeavyAutosElement={scrollToHeavyAutosElement}
          scrollToHydraulicsService={scrollToHydraulicsService}
        />
        <div className="col-lg-10 hide-overflow">
          <TechnicalOperations />
          <AksialPumps messagesAksialPumps={messagesAksialPumps} />
          <div class="pb-3">
            <SteeringHydraulics
              messagesSteeringHydraulics={messagesSteeringHydraulics}
            />
            <DieselInjector messagesDieselInjector={messagesDieselInjector} />
            <HydroMortors messagesHydroMotors={messagesHydroMotors} />
            <HeaavyAutosElements
              messagesHeavyAutosElement={messagesHeavyAutosElement}
            />
            <HydraulicService
              messagesHydraulicsService={messagesHydraulicsService}
            />
            <AdditionalInformation />
          </div>
        </div>
      </div>
      <button
        className="btn btn-secondary btn-sm go-to-top"
        onClick={scrollToTop}
      >
        Нагоре
      </button>
    </div>
  );
};
