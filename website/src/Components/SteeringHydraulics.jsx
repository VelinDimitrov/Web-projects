import React from "react";

export const SteeringHydraulics = ({ messagesSteeringHydraulics }) => (
  <React.Fragment>
    <h1 class="display-4 hydraulics-reiki" ref={messagesSteeringHydraulics}>
      Хидравлични рейки
    </h1>
    <p class="lead">
      Ремонта се извършва чрез подмяна с оригинални семеринги втулки, при нужда
      шлайфане на износените елементи в рейката, изпитване на стенд под високо
      налягане. Подмяна на втулки и лагери и механично изпитване на рейката
    </p>
    <div class="col">
      <span class="d-block text-muted py-3 razpredeliteli">
        Хидравлични разпределители с електро-магнитно управление, 12V или 24V.
      </span>
      <ul class="list-unstyled">
        <li class="list-group-item">Вграден предпазен клапан до 240 бара</li>
        <li class="list-group-item">Ограничител за края на хода</li>
        <li class="list-group-item">Максимално работно налягане 240 бара</li>
        <li class="list-group-item">
          Възможност за директен монтаж на резервоара
        </li>
        <li class="list-group-item">Дебит до 50 литра/мин</li>
      </ul>
    </div>
    <img
      class="d-block img-fluid pl-3"
      src="./img/autoshop/razpredeliteli1.jpg"
      alt="sf"
    />
    <div class="col">
      <span class="d-block text-muted py-3">
        Хидравлични разпределители с механично управление
      </span>
      <ul class="list-unstyled">
        <li class="list-group-item">Вграден предпазен клапан до 350 бара</li>
        <li class="list-group-item">Ограничител за края на хода</li>
        <li class="list-group-item">Максимално работно налягане 300 бара</li>
        <li class="list-group-item">
          Възможност за директен монтаж на резервоара
        </li>
        <li class="list-group-item">Дебит до 40 литра/мин</li>
      </ul>
    </div>
    <img
      class="d-block img-fluid pl-3"
      src="./img/autoshop/razpredeliteli2.jpg"
      alt="sf"
    />
    <div class="col">
      <span class="d-block text-muted py-3">
        Хидравлични разпределители с пневматично или механично управление
      </span>
      <ul class="list-unstyled">
        <li class="list-group-item">Вграден предпазен клапан до 350 бара</li>
        <li class="list-group-item">Ограничител за края на хода</li>
        <li class="list-group-item">Максимално работно налягане 250 бара</li>
        <li class="list-group-item">
          Възможност за директен монтаж на резервоара
        </li>
        <li class="list-group-item">Дебит до 70 литра/мин</li>
      </ul>
    </div>
    <img
      class="d-block img-fluid pl-3"
      src="./img/autoshop/razpredeliteli3.jpg"
      alt="sf"
    />
    <div class="col">
      <span class="d-block text-muted py-3">
        Хидравлични разпределители с пневматично управление
      </span>
      <ul class="list-unstyled">
        <li class="list-group-item">Вграден предпазен клапан до 350 бара</li>
        <li class="list-group-item">Ограничител за края на хода</li>
        <li class="list-group-item">Максимално работно налягане 350 бара</li>
        <li class="list-group-item">
          Възможност за директен монтаж на резервоара
        </li>
        <li class="list-group-item">Дебит до 130 литра/мин</li>
      </ul>
    </div>
    <img
      class="d-block img-fluid pl-3"
      src="./img/autoshop/razpredeliteli4.jpg"
      alt="sf"
    />
    <div class="col">
      <span class="d-block text-muted py-3">
        Хидравлични разпределители с пневматично управление
      </span>
      <ul class="list-unstyled">
        <li class="list-group-item">Вграден предпазен клапан до 350 бара</li>
        <li class="list-group-item">Ограничител за края на хода</li>
        <li class="list-group-item">Максимално работно налягане 350 бара</li>
        <li class="list-group-item">
          Възможност за директен монтаж на резервоара
        </li>
        <li class="list-group-item">Дебит до 180 литра/мин</li>
      </ul>
    </div>
    <img
      class="d-block img-fluid pl-3"
      src="./img/autoshop/razpredeliteli6.jpg"
      alt="sf"
    />
    <div class="col">
      <span class="d-block text-muted py-3">
        Хидравлични разпределители с пневматично управление Hyperion 13
      </span>
      <ul class="list-unstyled">
        <li class="list-group-item">Вграден предпазен клапан до 350 бара</li>
        <li class="list-group-item">Ограничител за края на хода</li>
        <li class="list-group-item">Максимално работно налягане 350 бара</li>
        <li class="list-group-item">
          Възможност за директен монтаж на резервоара
        </li>
        <li class="list-group-item">Дебит до 130 литра/мин</li>
      </ul>
    </div>
    <img
      class="d-block img-fluid pl-3"
      src="./img/autoshop/razpredeliteli7.jpg"
      alt="sf"
    />
    <div class="col">
      <span class="d-block text-muted py-3">
        Хидравлични разпределители с пневматично управление Hyperion 30
      </span>
      <ul class="list-unstyled">
        <li class="list-group-item">Вграден предпазен клапан до 350 бара</li>
        <li class="list-group-item">Ограничител за края на хода</li>
        <li class="list-group-item">Максимално работно налягане 350 бара</li>
        <li class="list-group-item">
          Възможност за директен монтаж на резервоара
        </li>
        <li class="list-group-item">Дебит до 200 литра/мин</li>
      </ul>
    </div>
    <img
      class="d-block img-fluid pl-3"
      src="./img/autoshop/razpredeliteli8.jpg"
      alt="sf"
    />
  </React.Fragment>
);
