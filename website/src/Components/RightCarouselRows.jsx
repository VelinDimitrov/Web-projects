import React from "react";
import { RightSec } from "./RightSec";

export const RightCarouselRows = (props) => {
  return (
    <div
      id="img-grid"
      className="col-lg-6 col-md-12 col-xs-12 col-lg-6-special"
    >
      <div className="row mb-5">
        <RightSec
          name="Ремонт на коли"
          src="./img/box-1.jpeg"
          alt="Ремонт на коли"
        />

        <RightSec
          name="Ремонт на скоростни кутии, диференциали"
          src="./img/gear.jpeg"
          alt="Ремонт на скоростни кутии, диференциали"
        />
      </div>

      <div className="row">
        <RightSec
          src={"./img/cylinder.jpeg"}
          name={"Ремонт на двигатели, рейки"}
          alt="Ремонт на двигатели, рейки"
        />

        <RightSec src={"./img/nozzle.jpg"} name={"ГНП Дюзи"} alt="ГНП Дюзи" />
      </div>
    </div>
  );
};
