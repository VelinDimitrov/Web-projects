import React from "react";

export const AdditionalInformation = () => (
  <div class="col">
    <h1 class="display-5">Допълнителна информация</h1>
    <p class="lead">
      Фирмата е специализирана в ремонта и поддръжката на хидросистемите на
      мобилна и стационарна техника. Ремонтира всички видове хидравлични помпи,
      мотори, разпределители и изпълнителни устройства на:
    </p>
    <ul class="list-unstyled">
      <li class="list-group-item">Багери</li>
      <li class="list-group-item">Челни товари</li>
      <li class="list-group-item">Булдозери</li>
      <li class="list-group-item">Ескаватори</li>
      <li class="list-group-item">Гредери</li>
      <li class="list-group-item">Асфалторазстилачи</li>
      <li class="list-group-item">Валяци</li>
      <li class="list-group-item">Гондоли</li>
      <li class="list-group-item">Вишки и повдигателни платформи</li>
      <li class="list-group-item">Трактори</li>
      <li class="list-group-item">Самоходни шасита и други</li>
    </ul>
  </div>
);
