import React from "react";
import { Link } from "react-router-dom";
import {
  GoogleMap,
  Marker,
  withGoogleMap,
  withScriptjs,
} from "react-google-maps";

export const Footer = ({ messagesEndRef }) => {
  return (
    <footer
      className="page-footer font-small bg-dark text-white pt-4 mt-4"
      ref={messagesEndRef}
    >
      <div className="container-fluid text-center text-md-left">
        <div className="row">
          <CompanyInfo />
          <div className="col-md-5 mt-md-0 mt-3">
            <MyMapComponent
              isMarkerShown
              googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={
                <div
                  style={{
                    height: `200px`,
                  }}
                />
              }
              mapElement={<div style={{ height: `100%`, width: "80%" }} />}
            />
          </div>
          <SeeMoreSection />
        </div>
      </div>
      <RightsSection />
    </footer>
  );
};

const RightsSection = (props) => {
  return (
    <div className="footer-copyright text-center py-3">
      &copy; <span className="year">{new Date().getFullYear()}</span> Всички
      права запазени:
      <span className="text-white"> SanirLTD</span>
    </div>
  );
};
const CompanyInfo = (props) => {
  return (
    <div className="col-md-4 mt-md-0 mt-3 text-center">
      <h5 className="text-uppercase">Как да се свържете с нас</h5>
      <p>Тел.№ +359895537214</p>
      <p>Имейл: sanir@mail.bg</p>
      <p>
        Адрес: зад училище Панайот Волов, гр. Пловдив, ул.Кипарис До КАТ, 4027
        Пловдив
      </p>
      <p>
        <a href="/" style={{ color: "#fff", marginRight: "0.5rem" }}>
          <i class="fa-brands fa-facebook"></i>
        </a>
        <a
          style={{ color: "#fff" }}
          href="https://www.google.com/maps/place/%D0%90%D0%B2%D1%82%D0%BE%D1%81%D0%B5%D1%80%D0%B2%D0%B8%D0%B7+-+Sanir+LTD+Service/@42.1620503,24.7216979,17z/data=!3m1!4b1!4m6!3m5!1s0x14acd14417e2345b:0xee529c2d129cfdbd!8m2!3d42.1620503!4d24.7216979!16s%2Fg%2F11ftk977fh"
        >
          <i class="fa-solid fa-location-dot"></i>
        </a>
      </p>
    </div>
  );
};

const SeeMoreSection = (props) => {
  return (
    <div className="col-md-3 mb-md-0 mb-3 text-center">
      <h5 className="text-uppercase">виж още</h5>
      <ul className="list-unstyled">
        <li>
          <Link to="/partners" className="text-white">
            Клиенти и Партньори
          </Link>
        </li>
      </ul>
    </div>
  );
};

const MyMapComponent = withScriptjs(
  withGoogleMap((props) => (
    <GoogleMap
      defaultZoom={15}
      defaultCenter={{ lat: 42.16263880021244, lng: 24.721569153978226 }}
    >
      <Marker position={{ lat: 42.16263880021244, lng: 24.721569153978226 }} />
    </GoogleMap>
  ))
);
