import React from "react";
import { Link } from "react-router-dom";

export const LowerCarouselSpec = (props) => {
  return (
    <div class="col-md-3 col-xs-12">
      <div class="hovereffect">
        <img
          className="w-100 h-100 img-thumbnail d-block img-fluid"
          src={props.src}
          alt={props.name}
        />
        <div class="overlay">
          <h2>Ремонт на Хидравлични бордове</h2>
          <Link className="info" to={"/shop"}>
            Отведи ме
          </Link>
        </div>
      </div>
    </div>
  );
};
