import React from "react";
import { Link } from "react-router-dom";

export const AutoShopSideBar = ({
  scrollToAksialPumps,
  scrollToSteeringHydraulics,
  scrollToDieselInjector,
  scrollToHydroMotors,
  scrollToHeavyAutosElement,
  scrollToHydraulicsService,
}) => (
  <div className="col-lg-2 m-0 p-0">
    <ul className="nav flex-column bg-secondary list-unstyled h-100 w-100">
      <li className="nav-item">
        <Link
          to="#"
          className="nav-link active text-white"
          onClick={scrollToAksialPumps}
        >
          Аксиално бутални хидравлични помпи
        </Link>
      </li>
      <li className="nav-item">
        <Link
          to="#"
          className="nav-link active text-white"
          onClick={scrollToSteeringHydraulics}
        >
          Хидравлични рейки
        </Link>
      </li>
      <li className="nav-item">
        <Link
          to="#"
          className="nav-link active text-white"
          onClick={scrollToSteeringHydraulics}
        >
          Разпределители
        </Link>
      </li>
      <li className="nav-item">
        <Link
          to="#"
          className="nav-link active text-white"
          onClick={scrollToDieselInjector}
        >
          Дизелови ГНП и дюзи
        </Link>
      </li>
      <li className="nav-item">
        <Link
          to="#"
          className="nav-link active text-white"
          onClick={scrollToHydroMotors}
        >
          Хидравлични помпи
        </Link>
      </li>
      <li className="nav-item">
        <Link
          to="#"
          className="nav-link active text-white"
          onClick={scrollToHydroMotors}
        >
          Хидро мотори
        </Link>
      </li>
      <li className="nav-item">
        <Link
          to="#"
          className="nav-link active text-white"
          onClick={scrollToHeavyAutosElement}
        >
          Товарни автомобили и ремаркета
        </Link>
      </li>
      <li className="nav-item">
        <Link
          to="#"
          className="nav-link active text-white"
          onClick={scrollToHydraulicsService}
        >
          Хидравлика
        </Link>
      </li>
    </ul>
  </div>
);
