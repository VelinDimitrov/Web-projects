import React from "react";

export const Partners = (props) => (
  <React.Fragment>
    <div className="jumbotron jumbotron-fluid mt-5">
      <div className="container text-center">
        <h1>
          <i className="fas fa-handshake mr-4"></i>Нашите Клиенти и Партньори
        </h1>
        <p className="lead"></p>
      </div>
    </div>
    <section className="container" id="clients">
      <ul className="list-unstyled">
        <li className="p-3 display-4 text-center">
          ХМЦ АД
          <p className="lead"></p>
          <img className="partner-logo" src="./img/logos/hmc.png" alt="logo" />
        </li>
        <li className="p-3 display-4 text-center">
          АДЛЕР ЕООД
          <p className="lead"></p>
          <img
            className="partner-logo"
            src="./img/logos/adler.jpg"
            alt="logo"
          />
        </li>
        <li className="p-3 display-4 text-center">
          Ада Колор ООД
          <p className="lead"></p>
          <img
            className="partner-logo"
            src="./img/logos/ada-color.jpg"
            alt="logo"
          />
        </li>
        <li className="p-3 display-4 text-center">
          Юнимекс ЕООД
          <p className="lead"></p>
          <img
            className="partner-logo"
            src="./img/logos/unimeks.jpg"
            alt="logo"
          />
        </li>
      </ul>
    </section>
  </React.Fragment>
);
