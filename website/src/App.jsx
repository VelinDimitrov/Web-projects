import React, { useCallback, useRef, useLayoutEffect } from "react";
import { Routes, Route, useLocation } from "react-router-dom";
import { Specialisation } from "./Components/Specialisation";
import Header from "./Components/Header";
import AboutSection from "./Components/AboutSection";
import { Partners } from "./Components/Partners";
import { NavBar } from "./Components/NavBar";
import { Footer } from "./Components/Footer";
import { AutoShop } from "./Components/AutoShop";

const headings = [
  ["Имате проблем?", "Ние Имаме Решение."],
  ["За Нас", "Нашата Дейност и Приоритети"],
  ["Сервиз - Информация"],
  ["Галерия"],
];

export const App = () => {
  const messagesEndRef = useRef(null);

  const scrollToBottom = useCallback(() => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  }, [messagesEndRef]);

  const messagesTopRef = useRef(null);

  const scrollToTop = useCallback(() => {
    messagesTopRef.current.scrollIntoView({ behavior: "smooth" });
  }, [messagesTopRef]);

  const location = useLocation();
  useLayoutEffect(() => {
    document.documentElement.scrollTo(0, 0);
  }, [location.pathname]);

  return (
    <div ref={messagesTopRef}>
      <NavBar scrollToBottom={scrollToBottom} />
      <Routes>
        <Route
          exact
          path="/"
          element={<Header logo="./img/rsz_logo.png" headings={headings[0]} />}
        />
        <Route
          path="/about"
          element={<Header logo="./img/rsz_logo.png" headings={headings[1]} />}
        />
        <Route
          path="/shop"
          element={<Header logo="./img/rsz_logo.png" headings={headings[2]} />}
        />
        <Route
          path="/gallery"
          element={<Header logo="./img/rsz_logo.png" headings={headings[3]} />}
        />
      </Routes>
      <Routes>
        <Route exact path="/" element={<Specialisation />} />
        <Route path="/partners" element={<Partners />} />
        <Route path="/shop" element={<AutoShop scrollToTop={scrollToTop} />} />
        <Route path="/about" element={<AboutSection />} />
      </Routes>
      <Footer messagesEndRef={messagesEndRef} scrollToTop={scrollToTop} />
    </div>
  );
};
