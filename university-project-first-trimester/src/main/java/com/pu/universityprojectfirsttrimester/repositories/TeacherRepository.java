package com.pu.universityprojectfirsttrimester.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pu.universityprojectfirsttrimester.entities.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Integer> {

}
