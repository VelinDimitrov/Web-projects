package com.pu.universityprojectfirsttrimester.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pu.universityprojectfirsttrimester.entities.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {

}
