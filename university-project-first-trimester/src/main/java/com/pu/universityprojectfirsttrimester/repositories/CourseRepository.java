package com.pu.universityprojectfirsttrimester.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pu.universityprojectfirsttrimester.entities.Course;

public interface CourseRepository extends JpaRepository<Course, Integer> {

	List<Course> findAllByStudents_Id(Integer id);
}
