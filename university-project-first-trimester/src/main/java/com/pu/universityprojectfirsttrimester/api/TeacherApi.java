package com.pu.universityprojectfirsttrimester.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pu.universityprojectfirsttrimester.dto.TeacherDto;
import com.pu.universityprojectfirsttrimester.service.TeacherService;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/teachers")
@AllArgsConstructor
public class TeacherApi {

private TeacherService teacherService;

	
	@GetMapping
	public ResponseEntity<List<TeacherDto>> getAll(){
		return new ResponseEntity<List<TeacherDto>>(teacherService.getAll(), HttpStatus.OK);		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TeacherDto> getById(@PathVariable Integer id){
		return new ResponseEntity<TeacherDto>(teacherService.getById(id), HttpStatus.OK);		
	}
	

	@PostMapping
	public ResponseEntity<?> createTeacher(TeacherDto teacherDto) {
		List<String> errorMessages = getValidationErrorMessages(teacherDto);
		if (!errorMessages.isEmpty()) {
			return ResponseEntity.badRequest().body(errorMessages);
		}
		teacherService.save(teacherDto);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping()
	public ResponseEntity<?> editTeacher(TeacherDto teacherDto) {
		List<String> errorMessages = getValidationErrorMessages(teacherDto);
		if (!errorMessages.isEmpty()) {
			return ResponseEntity.badRequest().body(errorMessages);
		}
		teacherService.update(teacherDto);
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteTeacher(@PathVariable Integer id) {
		teacherService.delete(id);
		return ResponseEntity.ok().build();
	}

	public List<String> getValidationErrorMessages(TeacherDto teacherDto) {
		List<String> errorMessages = new ArrayList<>();
		if (StringUtils.isBlank(teacherDto.getFirstName())) {
			errorMessages.add("First name is mandatory!");
		}
		if (StringUtils.isBlank(teacherDto.getLastName())) {
			errorMessages.add("Last name is mandatory!");
		}

		return errorMessages;
	}
}
