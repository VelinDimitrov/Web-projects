package com.pu.universityprojectfirsttrimester.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pu.universityprojectfirsttrimester.dto.CourseDto;
import com.pu.universityprojectfirsttrimester.dto.CourseFilter;
import com.pu.universityprojectfirsttrimester.dto.CourseSelect;
import com.pu.universityprojectfirsttrimester.service.CourseService;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/courses")
@AllArgsConstructor
public class CourseApi {

	private CourseService courseService;

	@GetMapping
	public ResponseEntity<List<CourseDto>> getAll(CourseFilter courseFilter) {
		return new ResponseEntity<List<CourseDto>>(courseService.getAll(courseFilter), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<CourseDto> get(@PathVariable Integer id) {
		return new ResponseEntity<CourseDto>(courseService.get(id), HttpStatus.OK);
	}
	
	@GetMapping("/students/{studentId}")
	public ResponseEntity<List<CourseDto>> getByStudentId(@PathVariable Integer studentId) {
		return new ResponseEntity<List<CourseDto>>(courseService.getAllForStudent(studentId), HttpStatus.OK);
	}

	
	@GetMapping("/select")
	public ResponseEntity<List<CourseSelect>> getCoursesSelectOptions(CourseFilter courseFilter) {
		return new ResponseEntity<List<CourseSelect>>(courseService.getCourseSelectOptions(), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<?> createCourse(CourseDto courseDto) {
		List<String> errorMessages = getValidationErrorMessages(courseDto);
		if (!errorMessages.isEmpty()) {
			return ResponseEntity.badRequest().body(errorMessages);
		}
		courseService.save(courseDto);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping()
	public ResponseEntity<?> editCourse(CourseDto courseDto) {
		List<String> errorMessages = getValidationErrorMessages(courseDto);
		if (!errorMessages.isEmpty()) {
			return ResponseEntity.badRequest().body(errorMessages);
		}
		courseService.update(courseDto);
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteCourse(@PathVariable Integer id) {
		courseService.delete(id);
		return ResponseEntity.ok().build();
	}

	public List<String> getValidationErrorMessages(CourseDto courseDto) {
		List<String> errorMessages = new ArrayList<>();
		if (courseDto.getPrice() == null) {
			errorMessages.add("Price is mandatory!");
		}
		if (StringUtils.isBlank(courseDto.getTitle())) {
			errorMessages.add("Title is mandatory!");
		}

		return errorMessages;
	}
}
