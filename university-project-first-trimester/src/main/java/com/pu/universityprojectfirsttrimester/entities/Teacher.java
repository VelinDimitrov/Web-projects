package com.pu.universityprojectfirsttrimester.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "teacher")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Teacher  implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="firstName", length = 255, nullable = false)
	private String firstName;
	
	@Column(name="lastName", length = 255, nullable = false)
	private String lastName;
	
	@Column(name="available")
	private boolean available;

	@OneToMany(mappedBy = "teacher", fetch = FetchType.LAZY,orphanRemoval = false)
	private List<Course> courses;
		
}
