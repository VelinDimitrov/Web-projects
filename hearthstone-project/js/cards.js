const baseUrl = "https://omgvamp-hearthstone-v1.p.rapidapi.com";
const settings = {
  headers: {
    "x-rapidapi-host": "omgvamp-hearthstone-v1.p.rapidapi.com",
    "x-rapidapi-key": "4cc1ae9b92msh7bc8cd18d16cd6bp164c24jsn883e0206bdb5",
  },
  async: true,
  crossDomain: true,
};

let view = "list";
let cards = [];

function getCards() {
  cards = [];
  $("#card-list").hide();
  $("#error-alert").addClass("d-none");
  $("#card-loading").addClass("d-flex").removeClass("d-none");

  const { race, ...otherFilters } = getCurrentFilters();
  $.ajax({
    ...settings,
    method: "GET",
    url: `${baseUrl}/cards/races/${race}`,
    data: otherFilters,
  })
    .done((response) => {
      cards = response;
      renderCardsList(response);
      $("#card-list").show();
    })
    .fail((response) => {
      const $errorAlert = $("#error-alert");
      $errorAlert.text(response.responseJSON.message);
      $errorAlert.removeClass("d-none");
    })
    .always(() => {
      $("#card-loading").addClass("d-none").removeClass("d-flex");
    });
}

function renderCardsList(cards = []) {
  $movieList = $("#card-list");
  $movieList.empty();

  const maxResults = $("#max-result").val();
  cards
    .filter((card) => card.img)
    .reduce(
      (accumulator, card) =>
        accumulator.some((c) => c.name === card.name)
          ? accumulator
          : [...accumulator, card],
      []
    )
    .slice(0, maxResults)
    .forEach((cards) => {
      const $template = getCardTemplate(cards);
      $movieList.append($template);
    });
}

function getCardTemplate(card) {
  const templateSelector = `#card-${view}-template`;
  const $template = $($(templateSelector).html());
  $template.find(".card-name").text(card.name);
  const image = card.img ?? "";
  $template.find(".card-image").attr("src", image);

  $template.find(".card-set").text(card.cardSet);

  const description =
    card.text &&
    card.text
      .replaceAll("\\n", " ")
      .replaceAll("_", " ")
      .replaceAll("[x]", " ");
  $template.find(".card-description").html(description);
  $template.find(".card-race").text(card.race);
  return $template;
}

$("#grid-view").click((e) => {
  view = "grid";
  $(e.currentTarget).addClass("btn-primary").removeClass("btn-outline-primary");
  $("#list-view").addClass("btn-outline-primary").removeClass("btn-primary");
  renderCardsList(cards);
});

$("#list-view").click((e) => {
  view = "list";
  $(e.currentTarget).addClass("btn-primary").removeClass("btn-outline-primary");
  $("#grid-view").addClass("btn-outline-primary").removeClass("btn-primary");
  renderCardsList(cards);
});

function getCurrentFilters() {
  const race = $("#race-filter").val() ?? "Quilboar";
  const health = $("#card-health").val();
  const cost = $("#card-cost").val();
  const attack = $("#card-attack").val();

  const filterObject = { race, attack, health, cost };
  Object.keys(filterObject).forEach(
    (key) => filterObject[key] === "" && delete filterObject[key]
  );

  return filterObject;
}

getCards();
