package com.example.microservices.currencyexchangeservice.controller;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.microservices.currencyexchangeservice.CurrencyExchangeRepository;
import com.example.microservices.currencyexchangeservice.bean.CurrencyExchange;

@RestController
public class CurrencyExchangeController {
	
	private Logger logger = LoggerFactory.getLogger(CurrencyExchangeController.class);
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private CurrencyExchangeRepository currencyExchangeRepository;
	
	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public CurrencyExchange getCurrencyExchange(@PathVariable String from, @PathVariable String to)  {
		logger.info("called with parameters {}  {}",from,to);
		
		CurrencyExchange currencyExchangeBean = currencyExchangeRepository.findByFromAndTo(from, to);
	
		if (currencyExchangeBean == null) {
			throw new RuntimeException("Unable to find data!");
		}
		
		currencyExchangeBean.setEnvironment(environment.getProperty("local.server.port"));
		return currencyExchangeBean;
	}

}
