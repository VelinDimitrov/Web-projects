package com.example.microservices.currencyexchangeservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.resilience4j.bulkhead.annotation.Bulkhead;

@RestController
public class CircuitBrakerController {
	
	private Logger logger = LoggerFactory.getLogger(CircuitBrakerController.class);
	
	@GetMapping("/sample-api")
	//@Retry(name="sample-api", fallbackMethod = "hardcodedResponse")
	//@CircuitBreaker(name="default", fallbackMethod = "hardcodedResponse")
	//@RateLimiter(name="default")
	//@Bulkhead(name="default")
	public String sampleApi() {
//		logger.info("Sample api call");
//		ResponseEntity<String> forEntity = new RestTemplate().getForEntity("localhost:8080/dummy-api", String.class);
		return "sample-api";
	}
	
	
	private String hardcodedResponse(Exception ex) {

		return "fallback response";
	}
}
